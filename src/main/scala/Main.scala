
import org.apache.spark._

object Main {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
      .setMaster("local[1]")
      .setAppName("Simple Spark Application")


    val sc = new SparkContext(conf)
    val count = sc.parallelize(Seq("Hello", "from", "Spark"), 1).count()

    println("------------------------------------------------------------------------------------------------")
    println(s"Count result: $count")
    println("------------------------------------------------------------------------------------------------")

    sc.stop()
  }

}
